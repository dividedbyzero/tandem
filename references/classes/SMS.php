<?php

class SMS
{
	protected static $url = "https://post.chikka.com/smsapi/request";
	protected static $clientID = "d6f767adbd868ffd553b23c26fa0c3f6030a8a69cf22028b4e96988f9b936309"; 
	protected static $secretKey = "521ec29ec26a1908537095c3875737714dcef336fea91e2f9d077d1571d5dfc5";
	protected static $requestCost = "FREE";
	protected static $shortcode = "2929025252";

//	protected static $appID = "87bat8ABpLuX8T7z7KiBpKu5q7a8tGx5";
//	protected static $secretKey = "93c2c08ceaa735d1466ef42177babf8361424c28eea224b00a75982b46d142cf";
//	protected static $shortcode = "1013";
	
	protected $message;
	protected $to;
	protected $requestID;
	protected $id;
	
	public static function Make($id)
	{
		$sms = new SMS;
		$sms->ID($id);
		
		return $sms;
	}
	
	public function To($to)
	{
		$this->to = $to;
		return $this;
	}
	
	public function Message($message)
	{
		$this->message = $message;
		return $this;
	}
	
	public function ID($id)
	{
		$this->id = $id;
		return $this;
	}
	
	public function Send()
	{	
		$message = $this->message;
		$id = $this->id;
		$to = $this->to;	
		
		// Prepare the parameters
		$params = [
	      "client_id" => static::$clientID,
	      "secret_key" => static::$secretKey,
	      "request_cost" => static::$requestCost,
	      "shortcode" => static::$shortcode,
	      "message" => $message,
	      "message_id" => $id,
	      "mobile_number" => $to,
	      "message_type" => "SEND"
		];
		
		return $this->http($params);
	}
	
	public function ReplyTo($requestID)
	{	
		$message = $this->message;
		$id = $this->id;
		$to = $this->to;	
		
		// Prepare the parameters
		$params = [
	      "client_id" => static::$clientID,
	      "secret_key" => static::$secretKey,
	      "request_cost" => static::$requestCost,
	      "shortcode" => static::$shortcode,
	      "message" => $message,
	      "message_id" => $id,
	      "request_id" => $requestID,
	      "mobile_number" => $to,
	      "message_type" => "REPLY"
		];
		
		return $this->http($params);
	}
	
	protected function http($params=null)
	{		
		//$url = "https://devapi.globelabs.com.ph/smsmessaging/v1/outbound/{static::$shortcode}/requests";
		
		// Parse the parameters
		$paramsString = "";
		foreach($params as $key => $value) { $paramsString .= $key.'='.urlencode($value).'&'; }
		rtrim($paramsString, '&');
		
		// HTTP Request
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, static::$url);
		curl_setopt($ch, CURLOPT_POST, count($params));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $paramsString);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$result = curl_exec($ch);
		
		// Close the connection
		curl_close($ch);
		
		// Return the result
		return $result;
	}
}

?>