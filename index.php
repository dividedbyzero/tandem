<?php

/**
 * Warp Framework
 * @author Jake Josol
 * @copyright MIT License
 */

require_once "references/references.php";

/****************************************
INITIALIZE - Prepare the application
****************************************/
App::Meta()
	->Title("Tandem")
	->Subtitle("Working hand in hand")
	->Description("Project gamification and member engagement platform")
	->Keywords(array("tandem", "sangguniang kabataan", "government"));

App::Environment()
	->Add("localhost:811", "development")
	->Add("dztnd.azurewebsites.net", "production");

/****************************************
START - Start the application
****************************************/
App::Start();

?>