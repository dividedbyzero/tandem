<?php

$type = Input::FromGet("type");
$desc = "Technical Skills";
switch($type)
{
	case "hunger": $desc = "Hunger Reduction"; break;
	case "innovate": $desc = "Innovation"; break;
}

?>
	
<h1><span class="fa fa-pencil"></span> New SK Project</h1>
<p class="lead">Create a new SK Project for &nbsp;<strong class="badge" style="background:orange"><?=$desc?></strong></p>
<hr>
<br>
<form class="form form-horizontal" action="<?=URL::To("project/submit")?>" method="post">
	<input type="hidden" name="mobile_number" value="639328873836">
	<div class="form-group">
		<label for="title" class="form-label">Title</label>
		<input class="form-control" type="text" name="title">
	</div>
	<div class="form-group">
		<label for="description" class="form-label">Description</label>
		<input class="form-control" type="text" name="description">
	</div>
	<div class="form-group">
		<label for="summary" class="form-label">Summary</label>
		<textarea class="form-control" type="text" name="summary"></textarea>
	</div>	
	<div class="form-group">
		<label for="message" class="form-label">Message</label>
		<textarea class="form-control" type="text" name="message"></textarea>
	</div>
	<button class="btn btn-success">Save</button>
</form>