<?php

$query = new Query("Proposal");
$result = $query->Find();

$query2 = new Query("_user");
$totalMembers = count($query2->Find());

?>
<h1><span class="fa fa-users"></span>&nbsp;&nbsp; <strong style="font-size:32pt">Sangguniang Kabataan 116</strong></h1>
<p class="lead">Valero St., Salcedo Village &nbsp;&nbsp;<span class="badge">122 members</span>&nbsp;<span class="badge" style="background:green"><?=$totalMembers?> active members</span></p>
<hr>
	<br>
	<center style="display:block; width:100%; overflow:hidden"><canvas id="myChart" width="1000" height="200"></canvas></center>
	<script>
			var ctx = document.getElementById("myChart").getContext("2d");
			var data = {
	    labels: ["January", "February", "March", "April", "May", "June", "July"],
	    datasets: [
	        {
	            label: "My First dataset",
	            fillColor: "rgba(220,220,220,0.2)",
	            strokeColor: "rgba(220,220,220,1)",
	            pointColor: "rgba(220,220,220,1)",
	            pointStrokeColor: "#fff",
	            pointHighlightFill: "#fff",
	            pointHighlightStroke: "rgba(220,220,220,1)",
	            data: [65, 59, 80, 81, 56, 55, 40]
	        },
	        {
	            label: "My Second dataset",
	            fillColor: "rgba(151,187,205,0.2)",
	            strokeColor: "rgba(151,187,205,1)",
	            pointColor: "rgba(151,187,205,1)",
	            pointStrokeColor: "#fff",
	            pointHighlightFill: "#fff",
	            pointHighlightStroke: "rgba(151,187,205,1)",
	            data: [28, 48, 40, 19, 86, 27, 90]
	        }
	    ]
	};
	var myLineChart = new Chart(ctx).Line(data, {

    ///Boolean - Whether grid lines are shown across the chart
    scaleShowGridLines : true,

    //String - Colour of the grid lines
    scaleGridLineColor : "rgba(0,0,0,.05)",

    //Number - Width of the grid lines
    scaleGridLineWidth : 1,

    //Boolean - Whether to show horizontal lines (except X axis)
    scaleShowHorizontalLines: true,

    //Boolean - Whether to show vertical lines (except Y axis)
    scaleShowVerticalLines: true,

    //Boolean - Whether the line is curved between points
    bezierCurve : true,

    //Number - Tension of the bezier curve between points
    bezierCurveTension : 0.4,

    //Boolean - Whether to show a dot for each point
    pointDot : true,

    //Number - Radius of each point dot in pixels
    pointDotRadius : 4,

    //Number - Pixel width of point dot stroke
    pointDotStrokeWidth : 1,

    //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
    pointHitDetectionRadius : 20,

    //Boolean - Whether to show a stroke for datasets
    datasetStroke : true,

    //Number - Pixel width of dataset stroke
    datasetStrokeWidth : 2,

    //Boolean - Whether to fill the dataset with a colour
    datasetFill : true,

    //String - A legend template
    legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"

});
	</script>
<hr>
<br>
<br>
<h3><span class="fa fa-certificate"></span> Projects</h3><br>
<table class="table table-striped table-bordered">
	<thead>
		<td><b>Project</b></td>
		<td><b>Description</b></td>
		<td><b>Actions</b></td>
	</thead>
	<?php foreach($result as $item): ?>
	<tr>
		<td><?=$item["title"]?></td>
		<td><?=$item["description"]?></td>
		<td><a class="btn btn-success" href="<?=URL::To("event")?>">View Participants</a></td>
	</tr>
	<?php endforeach;?>
</table>
<hr><br>
<h3><span class="fa fa-star"></span> Challenges</h3><br>
<table class="table table-striped table-bordered">
	<thead>
		<td><b>Organization</b></td>
		<td><b>Challenge</b></td>
		<td><b>Action</b></td>
	</thead>
	<tr>
		<td>Microsoft Corporation</td>
		<td>Develop Technical Skills</td>
		<td><a class="btn btn-info" href="<?=URL::To("project?type=tech")?>">Propose a Project</a></td>
	</tr>	
	<tr>
		<td>Mcdonald's (Golden Arches Mfg.)</td>
		<td>Fight Hunger</td>
		<td><a class="btn btn-info" href="<?=URL::To("project?type=hunger")?>">Propose a Project</a></td>
	</tr>	
	<tr>
		<td>Globe Labs</td>
		<td>Promote innovation</td>
		<td><a class="btn btn-info" href="<?=URL::To("project?type=innovate")?>">Propose a Project</a></td>
	</tr>
</table>
<style>
.btn-info
{
	width:200px;
}
</style>