<?php
	$query = New Query("attendees");
	$result = $query->Find();
?>
<h1>SK Computer Literacy Program</h1>
<p>By SK 116</p>
<hr
<br>
<h3><span class="fa fa-users"></span> Participants</h3>
<table class="table table-striped table-bordered">
	<thead>
		<td>Member</td>
		<td>Status</td>
	</thead>
	<?php foreach($result as $item): ?>
	<tr>
		<td><?=$item["firstName"]?> <?=$item["lastName"]?></td>
		<td><span class="badge">Attending</span></td>
	</tr>
	<?php endforeach; ?>
</table>