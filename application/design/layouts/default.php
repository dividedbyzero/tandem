<html>
	<?=Head::Create()->Bootstrap()->Render()?>
	<body>
		<header class="navbar navbar-inverse">
			<section class="col-md-10 col-md-offset-1">
				<div class="navbar-brand"><img height="25" src="<?=Resource::Local("resources/images/tandem-logo.png")?>"></div>
				<div class="navbar-right">
					<ul class="nav navbar-nav">
						<li><a href="<?=URL::To("#")?>">HOME</a></li>
						<li><a href="<?=URL::To("project")?>">PROJECT</a></li>
					</ul>
				</div>
			</section>
		</header>
		<br>
		<section class="col-md-10 col-md-offset-1" style="background:#fff; padding:40px; box-shadow:1px 1px 5px rgba(0,0,0,0.2)">
			<article class="content">
				<?=$data->Page->Render()?>
			</article>
		</section>
		<style>
			body
			{
				background:#efefef;
			}
		</style>
	</body>
</html>