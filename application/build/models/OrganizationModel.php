<?php 

class OrganizationModel extends Model
{
	protected static $source = "organization";
	protected static $key = "id";
	protected static $fields = array();

	protected static function build()
	{
		self::Has("id")->Increment();
		self::Has("name");
		self::Has("code");
	}
}

 ?>