<?php 

class MessageModel extends Model
{
	protected static $source = "message";
	protected static $key = "id";
	protected static $fields = array();

	protected static function build()
	{
		self::Has("id")->Increment();
		self::Has("mobile");
		self::Has("message");
		self::Has("type");
		self::Has("requestID");
		self::Has("status");
	}
	
	public static function Receive($sender, $message, $requestID)
	{
		$msg = new MessageModel;
		$msg->mobile = $sender;
		$msg->message = $message;
		$msg->requestID = $requestID;
		$msg->type = "INBOUND";
		$msg->status = "PENDING";
		$msg->Save();
		
//		$keywords = new PatternList;
//		
//		$keywords
//		->AddPattern("/SK116/", function($params=null)
//		{
//			return "You have successfully registered for Sangguniang Kabataan 116.";
//		})
//		->AddPattern("/YES/", function($params=null)
//		{
//			return "You have successfully registered for the event.";
//		})
//		->SetDefault(function($params=null)
//		{
//			return "Sorry, invalid keyword. Please try again.";
//		});

		// Prepare parameters
		$sender = Input::FromPost("mobile_number");
		$fname = Input::FromPost("first_name");
		$lname = Input::FromPost("last_name");
		$id = uniqid().date("YmdHis");
		
		$msg = new UserModel;
		$msg->mobile = $sender;
		$msg->firstName = $fname;
		$msg->lastName = $lname;
		$msg->Save();		
		
		// Retrieve response
		$response = "SK: Welcome to Sangguniang Kabataan 116! Ikanlulugod ka po naming makasapi sa paglulunsad ng mga proyektong pangkomunidad!";
		
		SMS::Make($id)
			->To($sender)
			->Message($response)
			->Send();	
			
		return "SK: Welcome to Sangguniang Kabataan 116! Ikanlulugod ka po naming makasapi sa paglulunsad ng mga proyektong pangkomunidad!";
	}
	
	public static function Join($sender, $message, $requestID)
	{
		$msg = new MessageModel;
		$msg->mobile = $sender;
		$msg->message = $message;
		$msg->requestID = $requestID;
		$msg->type = "INBOUND";
		$msg->status = "PENDING";
		$msg->Save();
		
//		$keywords = new PatternList;
//		
//		$keywords
//		->AddPattern("/SK116/", function($params=null)
//		{
//			return "You have successfully registered for Sangguniang Kabataan 116.";
//		})
//		->AddPattern("/YES/", function($params=null)
//		{
//			return "You have successfully registered for the event.";
//		})
//		->SetDefault(function($params=null)
//		{
//			return "Sorry, invalid keyword. Please try again.";
//		});

		// Prepare parameters
		$sender = Input::FromPost("mobile_number");
		$fname = Input::FromPost("first_name");
		$lname = Input::FromPost("last_name");
		$id = uniqid().date("YmdHis");
		
		$msg = new AttendeeModel;
		$msg->mobile = $sender;
		$msg->firstName = $fname;
		$msg->lastName = $lname;
		$msg->Save();		
		
		// Retrieve response
		$response = "SK: Salamat sa pagkumpirma! See you there!";
		
		SMS::Make($id)
			->To($sender)
			->Message($response)
			->Send();	
			
		return "SK: Salamt sa pagkumpirma! See you there!";
	}
}

 ?>