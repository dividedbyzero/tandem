<?php

/*
 * Attendee model
 * @author Jake Josol
 * @description Attendee model
 */
 
use Warp\Utils\Enumerations\SystemField;
use Warp\Utils\Enumerations\InputType;

class AttendeeModel extends Model
{
	protected static $source = "attendees";
	protected static $key = "id";
	protected static $fields = array();

	protected static function build()
	{
		self::Has("id")->Increment();
		self::Has("firstName")->String(50);	
		self::Has("lastName")->String(50);
		self::Has("mobile")->String(30);
	}
}

?>