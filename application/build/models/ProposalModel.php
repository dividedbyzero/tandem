<?php 

class ProposalModel extends Model
{
	protected static $source = "proposal";
	protected static $key = "id";
	protected static $fields = array();

	protected static function build()
	{
		self::Has("id")->Increment();
		self::Has("title");
		self::Has("description");
		self::Has("summary");
	}
}

 ?>