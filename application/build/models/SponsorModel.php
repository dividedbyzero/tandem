<?php 

class SponsorModel extends Model
{
	protected static $source = "sponsor";
	protected static $key = "id";
	protected static $fields = array();

	protected static function build()
	{
		self::Has("id")->Increment();
		self::Has("message");
		self::Has("sender");
		self::Has("receiver");
		self::Has("status");
		self::Has("response");
	}
}

 ?>