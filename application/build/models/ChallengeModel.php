<?php 

class ChallengeModel extends Model
{
	protected static $source = "challenge";
	protected static $key = "id";
	protected static $fields = array();

	protected static function build()
	{
		self::Has("id")->Increment();
		self::Has("title");
		self::Has("description");
		self::Has("budget");
		self::Has("sponsorID");
	}
}

 ?>