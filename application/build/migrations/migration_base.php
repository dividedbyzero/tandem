<?php

/*
 * Base migration
 * @author Jake Josol
 * @description Base migration
 */

use Warp\Utils\Interfaces\IMigration;

class migration_base implements IMigration
{
	public function Up()
	{
		Schema::Table("_user")
			->ID()
			->String("username")
			->Password()
			->String("firstName", 50)
			->String("lastName", 50)
			->String("email")
			->String("mobile")
			->String("orgCode")
			->String("accessToken")
			->Integer("roleID")
			->SessionToken()
			->Timestamps()
			->Create();
			
		Schema::Table("message")
			->ID()
			->String("mobile")
			->String("message", 250)
			->String("type")
			->String("requestID")
			->String("status")
			->Timestamps()
			->Create();
			
		Schema::Table("attendees")
			->ID()
			->String("firstName", 50)
			->String("lastName", 50)
			->String("mobile")
			->Timestamps()
			->Create();
			
		Schema::Table("proposal")
			->ID()
			->String("title")
			->String("description")
			->String("summary", 150)
			->Timestamps()
			->Create();
	}

	public function Down()
	{
		Schema::Table("_user")->Drop();
		Schema::Table("message")->Drop();
		Schema::Table("attendees")->Drop();
		Schema::Table("proposal")->Drop();
	}
}

?>