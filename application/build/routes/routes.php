<?php

/*
 * Routes
 * @author Jake Josol
 * @description Determines the routes used by the app
 */

/****************************************
IMPORTED ROUTES
****************************************/
Router::Import("api");
Router::Import("migration");

/****************************************
GENERAL ROUTES
****************************************/
Router::Home("HomeController");
Router::None("NotFoundController");

/****************************************
USER-DEFINED ROUTES
****************************************/
Router::Any("sms/permit", "SMSController@Permit");
Router::Any("sms/receive", "SMSController@Receive");
Router::Any("sms/notify", "SMSController@Notify");
Router::Any("sms/send", "SMSController@Send");
Router::Any("sms/join", "SMSController@Join");
Router::Any("project", function(){
	return View::Make()->Layout("default.php")->Page("project")->Render();
});
Router::Any("event", function(){
	return View::Make()->Layout("default.php")->Page("event")->Render();
});
Router::Post("project/submit", function()
{
	// Prepar parameters
	$sender = Input::FromPost("mobile_number");
	$title = Input::FromPost("title");
	$description = Input::FromPost("description");
	$summary = Input::FromPost("summary");
	$id = uniqid().date("YmdHis");
	$message = Input::FromPost("message");
	
	$prop = new ProposalModel;
	$prop->title = $title;
	$prop->description = $description;
	$prop->summary = $summary;
	$prop->Save();
	
	// Retrieve response
	$response = "SK:Bagong Proyekto! " . $title . " - " . $message . " - Reply 'JOIN' to Join the Event!";
	
	SMS::Make($id)
		->To($sender)
		->Message($response)
		->Send();
		
	Navigate::Within("#");
});


Router::Any("test", function()
{
	Schema::Table("attendees")
	->Drop();
});

?>