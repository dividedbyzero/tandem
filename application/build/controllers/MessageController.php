<?php

/*
 * Message controller
 * @author Jake Josol
 * @description Message controller
 */

use Warp\Utils\Traits\Controller\Apified;
use Warp\Utils\Traits\Controller\Crudified;

class MessageController extends Controller 
{
	use Apified, Crudified;
}

?>