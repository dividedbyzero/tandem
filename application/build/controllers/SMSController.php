<?php

class SMSController extends Controller
{
	public function PermitAction($parameters=null)
	{
		$user = new UserModel;
		$user->mobile = Input::FromPost("senderAddress");
		$user->accessToken = json_encode($_REQUEST);
		$user->Save();
		
		return Response::Make(200, "Success", "Successfully permited")->ToJSON();
	}
	
	public function ReceiveAction($parameters=null)
	{
		// Prepar parameters
		$sender = Input::FromPost("mobile_number");
		$requestID = Input::FromPost("request_id");
		$id = uniqid().date("YmdHis");
		$message = Input::FromPost("message");
		$timestamp = Input::FromPost("timestamp");
		
		// Retrieve response
		$response = "SK: ";
		$response = MessageModel::Receive($sender, $message, $requestID);
		
		// Retrieve result
//		$result = SMS::Make($id)
//					->To($sender)
//					->Message($response)
//					->ReplyTo($requestID);
		
		// Send response
		return Response::Make(200, "Success", array("API Response" => $result))->ToJSON();
	}
	
	public function JoinAction($parameters=null)
	{
		// Prepar parameters
		$sender = Input::FromPost("mobile_number");
		$requestID = Input::FromPost("request_id");
		$id = uniqid().date("YmdHis");
		$message = Input::FromPost("message");
		$timestamp = Input::FromPost("timestamp");
		
		// Retrieve response
		$response = "SK: ";
		$response = MessageModel::Join($sender, $message, $requestID);
		
		// Retrieve result
//		$result = SMS::Make($id)
//					->To($sender)
//					->Message($response)
//					->ReplyTo($requestID);
		
		// Send response
		return Response::Make(200, "Success", array("API Response" => $result))->ToJSON();
	}
	
	public function NotifyAction($parameters=null)
	{
		// Prepare parameters
		$messageType = Input::FromPost("message_type");
		$messageID = Input::FromPost("message_id");
		$status = Input::FromPost("status");
		$creditCost = Input::FromPost("credit_cost");
		$timestamp = Input::FromPost("timestamp");
		
		return Response::Make(200, "Success", "Ok")->ToJSON();
	}
	
	public function SendAction($parameters=null)
	{
		// Prepare parameters
		$to = Input::FromPost("mobile_number");
		$message = Input::FromPost("message");
		$id = uniqid().date("YmdHis");
		
		// Retrieve result
		$result = SMS::Make($id)
					->To($to)
					->Message($message)
					->Send();
		
		// Send response
		return Response::Make(200, "Success", array("API Response" => $result))->ToJSON();
	}
	
	
}

?>