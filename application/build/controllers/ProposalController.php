<?php

/*
 * User controller
 * @author Jake Josol
 * @description User controller
 */

use Warp\Utils\Traits\Controller\Apified;
use Warp\Utils\Traits\Controller\Crudified;

class ProposalController extends Controller 
{
	use Apified, Crudified;
}

?>